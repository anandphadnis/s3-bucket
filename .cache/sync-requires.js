const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/anandphadnis/Desktop/gatsby/gatsby-static/s3-bucket/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/anandphadnis/Desktop/gatsby/gatsby-static/s3-bucket/src/pages/404.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/anandphadnis/Desktop/gatsby/gatsby-static/s3-bucket/src/pages/index.js"))),
  "component---src-pages-page-2-js": hot(preferDefault(require("/Users/anandphadnis/Desktop/gatsby/gatsby-static/s3-bucket/src/pages/page-2.js"))),
  "component---src-pages-using-typescript-tsx": hot(preferDefault(require("/Users/anandphadnis/Desktop/gatsby/gatsby-static/s3-bucket/src/pages/using-typescript.tsx")))
}

